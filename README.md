# Flow（流程图绘制）

## 效果展示
![](./src/assets/flow.gif)

## 安装依赖
```
npm install
```

## 项目运行
```
npm run serve
```

## 项目打包
```
npm run build
```

## 开发探讨QQ群
![](./src/assets/flowClass.png)

## 个人博客站点

